import { TestBed } from '@angular/core/testing';

import { AddVisithistoryService } from './add-visithistory.service';

describe('AddVisithistoryService', () => {
  let service: AddVisithistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddVisithistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
